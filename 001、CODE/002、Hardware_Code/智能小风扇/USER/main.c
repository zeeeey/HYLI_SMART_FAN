#include "../FUNC/display_4X8.h"
#include "../FUNC/uart.h"
#include "../INTE/inte.h"

sbit DCOUT = P1^1;//定义电机信号输出端口
//------------------------------------------------
//全局变量
//------------------------------------------------
unsigned char PWM_ON;   //定义速度等级
#define CYCLE 10        //周期

//变量
extern unsigned char code DuanMa[];// 显示段码值
extern unsigned char TempData[]; //存储显示值的全局变量
extern unsigned char getByte[];          //定义临时变量
extern unsigned char flag; 			//接收标记
extern unsigned char point;			//指针

//函数
extern void Display(unsigned char FirstBit,unsigned char Num);//数码管显示函数
extern void Init_Timer0(void);//定时器初始化
extern void InitUART(void);
extern void SendStr(unsigned char *s);
extern void SendByte(unsigned char dat);

//------------------------------------------------
//主函数
//------------------------------------------------
void main (void)
{
	//发来的FF EE num AA 或 FF DD num AA返回 AA和FF互换位置
    unsigned char answer[5];
	unsigned char k,data1,data2;
    answer[0]=0xAA;
    answer[3]=0xFF;
	answer[4]='\0';
	TempData[2]=DuanMa[0]; //显示速度等级
	TempData[3]=DuanMa[0]; 	

	PWM_ON=0;
	InitUART();
	Init_Timer0();    //初始化定时器0，主要用于数码管动态扫描

	while (1)         //主循环
	{
		if(flag==1 && point>3 && getByte[point-4]==0xFF)
		{
			ES = 0;   //关串口中断

			answer[1]=0xFF;
			data1=getByte[point-3];
			data2=getByte[point-2];
			if(data1==0xEE){
				if(0<=data2 && data2<=10){
					PWM_ON=data2;
					TempData[2]=DuanMa[PWM_ON/10]; //显示速度等级
					TempData[3]=DuanMa[PWM_ON%10]; 	
					answer[1]=0xEE;
					answer[2]=data2+1;
				}
			}else if(data1==0xDD){
					answer[1]=0xDD;
					answer[2]=PWM_ON+1;
			}
			SendStr(answer);		//应答

			for(k=0;k<8;k++)		//清空getByte中数据
				getByte[k]=0;
			point=0;				//point归零
			flag=0;					//重置flag标志
			ES=1;					//打开串口中断
		}
	}
}

//------------------------------------------------
//定时器中断子程序
//------------------------------------------------
void Timer0_isr(void) interrupt 1 
{
	static unsigned char count;
	TH0=(65536-2000)/256;		  //重新赋值 2ms
	TL0=(65536-2000)%256;
	
	Display(0,4);                // 调用数码管扫描
	
	if (count==PWM_ON) 
    {
		DCOUT = 0;               //如果定时等于on的时间，
		//说明作用时间结束，输出低电平
    }
	count++;
	if(count == CYCLE)       //反之低电平时间结束后返回高电平
    {
		count=0;
		if(PWM_ON!=0)    //如果开启时间是0 保持原来状态
			DCOUT = 1;      	
    }
}

